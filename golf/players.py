import os, pathlib
import json

class Players(dict):
    def __init__(self, ppath):
        super().__init__()
        self.path = ppath
        if os.path.exists(ppath):
            with open(ppath, "r", encoding="utf-8") as pf:
                self.update(json.load(pf))
        else:
            pathlib.Path(ppath).parent.mkdir(parents=True, exist_ok=True)
            

    def save(self):
        with open(self.path, "w", encoding="utf-8") as f:
            json.dump(self, f)


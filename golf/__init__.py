#!/usr/bin/env python

import players, rounds, holes

def load_players(ppath):
    return players.Players(ppath)

def load_rounds(rpath):
    return rounds.Rounds(rpath)

def load_holes(hpath):
    return holes.Holes(hpath)

if __name__ == '__main__':
    print("This module should not be executed directly!")


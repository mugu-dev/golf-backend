import pathlib
import json

class Rounds(dict):
    def __init__(self, rpath):
        super().__init__()
        self.path = rpath
        
        if pathlib.Path(rpath).exists():
            with open(rpath, "r", encoding="utf-8") as rf:
                self.update(json.loads(rf.read()))
        else:
            pathlib.Path(rpath).parent.mkdir(parents=True, exist_ok=True)


    def save(self):
        with open(self.path, "w", encoding="utf-8") as f:
            json.dump(self, f)


import os
import glob
import json

class Holes(list):
    def __init__(self, hpath):
        super().__init__()
        self.path = hpath
        # Load hole information 
        holepaths = glob.glob(os.path.join(hpath, "*.json"))
        for holepath in holepaths:
            with open(holepath, "r", encoding="utf-8") as holefile:
                self.extend([hole for hole in json.loads(holefile.read())["holes"]])

